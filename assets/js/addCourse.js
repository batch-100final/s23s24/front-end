let formSubmit = document.querySelector('#createCourse');

formSubmit.addEventListener('submit', (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value;
	let price = document.querySelector('#coursePrice').value;

	let token = localStorage.getItem('token');

	fetch('http://localhost:3000/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
			})
		})
		.then(res => res.json())
			.then(data => {
					//creation of new course successful
					if(data === true) {
						//redirect to course page
						alert('Course Created!')
						window.location.replace('./courses.html')
					} else {
						alert('Something went wrong')
					}
		})
	})
