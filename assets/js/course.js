//window.location.search returns the query string part of the URL

// console.log(window.location.search)

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

// console.log(courseId)
let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDescription')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')

fetch(`http://localhost:3000/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary">
				Enroll
			</button>
		`
		document.querySelector("#enrollButton").addEventListener('click', () => {
			fetch('http://localhost:3000/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId : courseId
				})
			})
			.then(res => res.json())
			.then(data => {
					//creation of new course successful
					if(data === true) {
						//redirect to course page
						alert('Thank you for enrolling!')
						window.location.replace('./courses.html')
					} else {
						//redirect in creating course
						alert('Something went wrong')
					}
			})
		})

	})

