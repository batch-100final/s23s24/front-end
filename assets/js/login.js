let loginForm = document.querySelector('#logInUser');

//event or e for shorthand 
//when it is triggered it will create an event object
loginForm.addEventListener('submit', (event) => {
	event.preventDefault();


	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	if(email =='' || password == '' ){
		alert("Please input email/pass");
	} else {
		fetch('http://localhost:3000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(res => {return res.json()})
		.then(data => {
			if(data.accessToken) {
				//store JWT in a local storage
				localStorage.setItem('token', data.accessToken);
				//send fetch request to decode JWT and obtain user ID and role for storing in context
				fetch('http://localhost:3000/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()})
				.then(data => {
					//set the global user to have properties containing authenticated users IS and role
				localStorage.setItem('id', data._id)
				localStorage.setItem('isAdmin', data.isAdmin)
				window.location.replace('./courses.html')
				})
			} else {
				alert("Something went wrong");
			}
		})

	}
})

