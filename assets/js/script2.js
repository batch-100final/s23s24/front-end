//Add the logic that will show the login button when a user is not logged in


let navItems = document.querySelector("#navSession")
console.log(navItems)

let userToken = localStorage.getItem("token")
console.log(userToken)

if(!userToken) {
	navItems.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item"> 
			<a href="./pages/register.html" class="nav-link"> Sign-up </a>
		</li>
		<li class="nav-item"> 
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
	</ul>
	`
} else {
	navItems.innerHTML = 
	`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"> 
				<a href="./pages/userProfile.html" class="nav-link"> Profile </a>
			</li>
			<li class="nav-item"> 
				<a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>
		</ul>
	`
}
