let adminUser = localStorage.getItem("isAdmin") === "true"; 
let params = new URLSearchParams(window.location.search)
let token = localStorage.getItem('token')
let courseId = params.get('courseId')

if(adminUser === true){
	fetch(`http://localhost:3000/api/courses/${courseId}`,{
	method:'GET',
	headers:{
	'Content-Type': 'application/json',
	'Authorization': `Bearer ${token}`
	}
})
	.then(res => res.json())
	.then(data => {

	let enrolleeIds = data.enrollees.map((enrollee)=>{
		return enrollee.userId
	})
for (let enrolleeId of enrolleeIds){
	fetch(`http://localhost:3000/api/users/${enrolleeId}`,{
		method:'GET',
		headers:{
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		const newLi =document.createElement('li')
		newLi.append(`${data.firstName} ${data.lastName}`)
		enrolleesContainer.append(newLi)		
	})
}
})
	
} else {
	fetch(`http://localhost:3000/api/users/details`,{
		method: 'GET',
					headers:{
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					}
	})
	.then(res => res.json())
			
	.then(data => {
			//GET the users data firstname,lastname,email,mobileno
			console.log(data)
			// console.log(data.firstName)
			// loop over the enrollments	
			let subjectData = data.enrollments
			let courseInfo = [];
			let subjectContainer =document.querySelector("#subjectContainer")

			for(let subject of subjectData){
				// console.log(subject.courseId)
				fetch(`http://localhost:3000/api/courses/${subject.courseId}`)
				.then(res => res.json())
				.then(data =>{

					subjectContainer.innerHTML ="";
					// console.log(data);
					courseInfo.push ({courseName:data.name, courseDescription:data.description});
					// console.log(courseInfo[0].courseDescription)
					for(let i = 0 ; i < courseInfo.length ; i++){
						console.log(courseInfo[i].courseName)
						const newLi = document.createElement('li');
						newLi.append(`${courseInfo[i].courseName} :${courseInfo[i].courseDescription}`);
						subjectContainer.append(newLi);
					}
				})
			}
			if(data){

				profileContainer.innerHTML =
					`	<h3>User Records</h3>
						<li>First name: ${data.firstName}</li>
						<li>Last name: ${data.lastName}</li>
						<li>Email: ${data.email}</li>
						<li>Mobile no: ${data.mobileNo}</li>
					`	
			}
	})
}
